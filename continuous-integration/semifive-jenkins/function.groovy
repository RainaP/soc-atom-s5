def configuration() {
    env.project	    = "soc-atom-s5"
    env.group       = "R4"
    env.gitlab      = "portal"
    env.module      = "semifivesnippet/atom"
    ciPath          = "/continuous-integration/semifive-jenkins"
    resultLog       = "regr_status.log"
    tgt             = ["c_compile/ctests", "compile", "execute"]
}

def path(){
    archivePath     = "/user/jenkins/archive"
    zshPath         = "/semifive/tools/Modules/default/init/zsh"
    tmp_pwd         = sh(script: "pwd", returnStdout:true).trim()
    tmp_basedir     = sh(script: "basename ${tmp_pwd}", returnStdout:true).trim()
    result_path     = "${tmp_pwd}/${env.project}/sim"
}

def variable(){
    test_type = "${params.testType}"
    if(test_type == "nightly"){
        source_branch = "master"
        target_branch = "master"
    }else{
        source_branch   = "$gitlabSourceBranch"
        target_branch   = "$gitlabTargetBranch"
    }
    pass = 0
    fail = 0     
    test_list = []
    dut_list = []
}

def getGitBranchName(){
    return sh(returnStdout: true, script: """echo "${scm.branches[0].name}" | awk -F "/" '{print \$2}'""").trim()
}

def checkBranch(){
    if(test_type == "nightly"){
        default_branch = "master"
    }else{
        default_branch = function.getGitBranchName()
        println(default_branch)    

        if(target_branch != default_branch){
            sh "exit 0"
        }
    }
}

def prepareGitWit(){
    sh """
       source ${zshPath}
       module load ${env.module}
       git clone git@${env.gitlab}:${env.group}/${env.project}.git -b ${default_branch}
       wit init .
       git -C ${env.project} merge origin/${target_branch} origin/${source_branch}
       wit add-pkg git@${env.gitlab}:${env.group}/${env.project}.git
       wit update-pkg ${env.project}
       wit update
    """
}

def runSim(){
    try{
        if(test_type == "nightly"){
            sh """
                source ${zshPath}
                module load ${env.module}
                source ${env.project}/${ciPath}/nightly-ci-command.sh
            """                 
        }else{
            timeout(time: 480, unit: 'MINUTES'){
                sh """
                    source ${zshPath}
                    module load ${env.module}
                    source ${env.project}/${ciPath}/ci-command.sh
                """ 
            }
        }        
    }catch(e){
        println("CI timeout")
    }  
}

def makeTestlist(){
    dir("${result_path}"){
        dir("execute"){
            sh """
                ls > result_test_list.log
                sed -i '/@tmp/d' result_test_list.log
                sed -i '/.log/d' result_test_list.log
                sed -i '/pre_regr_test_list.json/d' result_test_list.log
            """
            read_test = readFile 'result_test_list.log'
            tests = read_test.split('\n')
            
            for(each in tests){
                test_list.add(each as String)
            }
        }
        dir("compile"){
            read_dut = readFile 'regr_status.log'
            DUT = read_dut.split('\n')

            for(each in DUT){
                dut_list.add(each as String)
            } 
        }
    }
}

def checkFailure(){
    println("check failure")

    tgt.each{ itr->
        dir("${result_path}"){
            dir(itr){
                if(fileExists("${resultLog}")){
                    for(i = 0; i < dut_list.size(); i++){
                        if(read_dut.contains('FAILED')){
                            println("FAIL in '" +itr +"'")    
                            sh "exit 1"
                        }else if(!(dut_list[i].contains('FAILED')) && !(dut_list[i].contains('PASSED'))){
                            println("Neither FAIL nor PASS in '" +itr +"'")    
                            sh "exit 1"                        
                        }else{
                            println("PASS in '" +itr +"'")
                        }
                    }
                }else{
                    println("no result log in directory")
                }
            }
            for(i = 0; i < test_list.size(); i++){
                dir("execute/${test_list[i]}"){
                    if(fileExists('status.log')){
                        read_status = readFile 'status.log'
                        if ((read_status =~ /PASSED/)){
                            println("pass")
                        }else{
                            sh "exit 1"
                        }
                    }else{
                        println("no status.log in result directory")
                        sh "exit 1"
                    }                
                }
            }
        }
    }     
}     

def countResult(){     
    println("count result")
    if(default_branch == target_branch){
        tgt.each{itr->
            dir("${result_path}"){
                dir(itr){
                    if(fileExists("${resultLog}")){
                        result = sh(script: "cat ${resultLog}", returnStdout:true).trim()
                        total = sh(script: "cat ${resultLog} | wc -l", returnStdout:true).trim()
                        pass = sh(script: "grep -c 'PASSED' ${resultLog}", returnStdout:true).trim()
                        fail = sh(script: "grep -o 'FAILED' ${resultLog} | wc -l", returnStdout:true).trim()
                    }
                    println("Summary '" +itr +"'\n-----------------------------------------\n${result}\n-----------------------------------------")
                    println("Result '" +itr +"'\n-----------------------------------------\nTOTAL : ${total} | PASSED : ${pass} | FAILED : ${fail}\n-----------------------------------------")
                }
            }    
        } 
    }
}

def archive(){
    try{
        if (fileExists("${archivePath}/${JOB_NAME}") == false){
            sh "mkdir -p ${archivePath}/${JOB_NAME}"
        }
        sh "srun tar -czf ${archivePath}/${JOB_NAME}/${JOB_NAME}.${BUILD_NUMBER}.tar.gz --ignore-failed-read --exclude=.fuse ${tmp_pwd}"
    }catch(e){
        println("archive error")
    }
}

return this
