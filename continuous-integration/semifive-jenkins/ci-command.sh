#!/bin/bash

# CI command script
make clean -C soc-atom-s5/scripts
make tb -C soc-atom-s5/scripts
srun make -C soc-atom-s5/scripts TEST_MODE=regr TEST=pre_regr_test_list.json WAVE=false
